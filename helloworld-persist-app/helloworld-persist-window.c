/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>
#include "helloworld-persist-window.h"
#include "helloworld-persist-app.h"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwPersistAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *button, *button_box;
};

G_DEFINE_TYPE(HlwPersistAppWindow, hlw_persist_app_window, GTK_TYPE_APPLICATION_WINDOW);


static void
button_pressed_cb (GtkWidget *button,
                   gpointer user_data)
{
  g_autofree gchar *display_text = NULL;
  g_autofree gchar *state_file_path = NULL;
  gint count = 0;
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();

  HlwPersistAppWindow *self  = HLW_PERSIST_APP_WINDOW (user_data);

  state_file_path = g_strdup_printf("%s/org.apertis.HelloWorldPersistApp.ini", g_get_user_data_dir ());
		
  /* Load last updated time and context from perstent db
   * However, it's usually very expensive operation so
   * it shouldn't load and store too frequently.
   */
  if (!g_key_file_load_from_file (key_file, state_file_path,
      G_KEY_FILE_KEEP_COMMENTS,
      &error))
    {
      g_debug ("Failed to load key file[%s] (reason: %s)",
          state_file_path, error->message);
      g_clear_error (&error);

      goto store_db;
    }

  count = g_key_file_get_integer (key_file,
      "Button Event", "count",
      &error);
  if (error)
    {
      g_debug ("Failed to read 'count' (reason: %s)", error->message);
      g_clear_error (&error);

      goto store_db;
    }

  display_text = g_strdup_printf("Count: %d", count);

  gtk_button_set_label(GTK_BUTTON(self->button), display_text);

store_db:
  g_key_file_set_integer (key_file, "Button Event",
      "count", count + 1);

  if (!g_key_file_save_to_file (key_file,
      state_file_path,
      &error))
    {
      g_warning ("Failed to save db (reason: %s)", error->message);
      g_clear_error (&error);
    }
}

static void
hlw_persist_app_window_init (HlwPersistAppWindow *self)
{
  HlwPersistAppWindow *win = HLW_PERSIST_APP_WINDOW(self); 
  gtk_window_set_default_size(GTK_WINDOW(win),WIN_WIDTH,WIN_HEIGHT);
  win->button = gtk_button_new_with_label("Add Count!");
  g_signal_connect (win->button, "clicked", G_CALLBACK(button_pressed_cb), win);
  win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add (GTK_CONTAINER(win->button_box), win->button);
  gtk_container_add (GTK_CONTAINER(win), GTK_WIDGET(win->button_box));
  gtk_widget_show_all(GTK_WIDGET(win));
}

static void
hlw_persist_app_window_class_init (HlwPersistAppWindowClass *klass)
{
}

HlwPersistAppWindow *
hlw_persist_app_window_new (HlwPersistApp *app)
{
  return g_object_new (HLW_PERSIST_APP_WINDOW_TYPE, "application", app, NULL);
}

